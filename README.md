# About

SYSKEN Advent Calendar 2018のサイトをビルドするやつです  
[デモサイト](https://glatan.gitlab.io/sysken-advent-calendar-2018/)

# How to Use

1. git clone https://gitlab.com/glatan/sysken-advent-calendar-2018.git
2. cd sysken-advent-calendar-2018
3. npm install
4. npm run build

プロジェクトディレクトリ直下に "index.html" が出力されます。

# Config

それぞれ以下の形式になっている。

## src/config/days.pug

```js
[
  "日", "月", "火", "水", "木", "金", "土"
]
```

## src/config/post.pug

```js
{
  "投稿者名(SYSKEN ONLINE上のもの)": "記事名", "記事のリンク"
}
```

## src/config/writer.pug

```js
{
  "投稿者名(SYSKEN ONLINE上のもの)": "アイコンのリンク(SYSKEN ONLINE上のもの)"
}
```
